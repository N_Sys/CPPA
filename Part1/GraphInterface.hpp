#pragma once

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/property_map/property_map.hpp>
#include <utility>
#include <vector>

template <class Graph, class NodePropertyMap> class myGraphImage {
    using point_type =
        typename boost::property_traits<NodePropertyMap>::key_type;
    using value_type =
        typename boost::property_traits<NodePropertyMap>::value_type;
    using pixel_type = std::pair<point_type, value_type>;
    using reference = myGraphImage &;
    using const_reference = const myGraphImage &;

  private:
    const Graph &graph;
    const NodePropertyMap &properties;

  public:
    myGraphImage(const Graph &g, const NodePropertyMap &values)
        : graph(g), properties(values) {}

    const_reference operator()(point_type p) const {
        return get(properties, p);
    }
    reference operator()(point_type p) { return get(properties, p); }

  // vectors are here for testing purpose
  // TODO: remplace vectors with ranges
    auto domain() const {
        auto res = std::vector<point_type>();
        for (auto vp = vertices(graph); vp.first != vp.second; ++vp.first)
            res.push_back(*vp.first);

        return res;
    }

    auto values() const {
        auto res = std::vector<value_type>();
        for (auto vp = vertices(graph); vp.first != vp.second; ++vp.first)
            res.push_back(properties[*vp.first]);

        return res;
    }

    auto values() {
        auto res = std::vector<value_type>();
        for (auto vp = vertices(graph); vp.first != vp.second; ++vp.first)
            res.push_back(properties[*vp.first]);

        return res;
    }

    auto pixels() const {
        auto res = std::vector<pixel_type>();
        for (auto vp = vertices(graph); vp.first != vp.second; ++vp.first)
            res.push_back(pixel_type(*vp.first, properties[*vp.first]));

        return res;
    }

    auto pixels() {
        auto res = std::vector<pixel_type>();
        for (auto vp = vertices(graph); vp.first != vp.second; ++vp.first)
            res.push_back(pixel_type(*vp.first, properties[*vp.first]));

        return res;
    }
};
