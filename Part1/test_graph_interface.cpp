#include "GraphInterface.hpp"
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/property_map/property_map.hpp>
#include <iostream>

int main() {
    using Graph = typename boost::adjacency_list<boost::vecS, boost::vecS,
                                                 boost::bidirectionalS, int>;
    // Make convenient labels for the vertices
    enum { A, B, C, D, E, N };
    const int num_vertices = N;

    // writing out the edges in the graph
    typedef std::pair<int, int> Edge;
    Edge edge_array[] = {Edge(A, B), Edge(A, D), Edge(C, A), Edge(D, C),
                         Edge(C, E), Edge(B, D), Edge(D, E)};
    const int num_edges = sizeof(edge_array) / sizeof(edge_array[0]);

    // declare a graph object
    Graph g(num_vertices);

    // add the edges to the graph object
    for (int i = 0; i < num_edges; ++i)
        add_edge(edge_array[i].first, edge_array[i].second, g);

    auto graph_image =
      myGraphImage<Graph, boost::property_map<Graph, boost::vertex_index_t>::type>(
            g, get(boost::vertex_index, g));


    for (auto elm : graph_image.domain())
      std::cout << elm << "; ";
    std::cout << std::endl;

    for (auto elm : graph_image.values())
      std::cout << elm << "; ";
    std::cout << std::endl;

    auto v = graph_image.values();
    v[2] = 5;

    for (auto elm : graph_image.values())
      std::cout << elm << "; ";
    std::cout << std::endl;

    for (auto elm : graph_image.pixels())
      std::cout << elm.first << " - " << elm.second << "; ";
    std::cout << std::endl;
}
