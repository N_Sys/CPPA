#pragma once
#include <boost/graph/adjacency_list.hpp>

template <typename Image, typename Gtype>
class Graph
{
  public :
    using vertex_descriptor = typename Image::value_type;
    using edge_descriptor = std::pair<int,int>;
    using directed_category = boost::undirected_tag;
    using edge_parallel_category = boost::disallow_parallel_edge_tag;
    using traversal_category = Gtype;

    Graph(Image& i);
    auto nodes_get(){return nodes_;}
    auto nodes_get() const {return nodes_;}
  private :
    std::map<int,vertex_descriptor> nodes_;
};
