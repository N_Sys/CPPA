#pragma once
#include "graph.hh"

template <class Image, class Gtype>
class IncidenceGraph: public Graph
{
  public :
    using traversal_category = boost::incidence_graph_tag;
    using out_edge_iterator =typename Graph<Image,Gtype>::edge_descriptor;
    using degree_size_type = int;
};
