#include "graph.hh"

template <class Image,class Gtype>
Graph<Image,Gtype>::Graph(Image& i)
{
  int curr = 0;
  for (auto pix : i.pixels())
  {
    nodes_.insert(std::pair<int,vertex_descriptor>(curr++,pix));
  }
}
