#pragma once

class ImageInterface
{
  using point_type = ...;
  using value_type = ...;
  using pixel_type = ...;
  using reference = ...;
  using const_reference = ...;

  const_reference operator() (point_type p) const;
  reference operator() (point_type p);

  auto domain() const;

  auto values() const;
  auto values();

  auto pixels() const;
  auto pixels();
};
